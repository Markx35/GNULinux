# Сравнение инструкций CMD и ENTRYPOINT в Dockerfile

С первого взгляда при чтении официальной документации к Docker, сложно понять чем разнятся `ENTRYPOINT` и `CMD` в `Dockerfile`, ведь обе этих инструкции могут описывать входную точку контейнера, то есть бинарный файл с указанием параметров, который будет запущен. Таким образом мы запускаем сервис.

До тех пор пока контейнер не будет использоваться в каком-нибудь серьезном CI или предназначен для среды разработки, а так же каких-либо других серьезных целей, разница скорее не будет прочувствована. Но первые ошибки вроде попыток "перебить" с помощью `CMD` "железного" `ENTRYPOINT`, в котором прописан запуск какого-то бинарного файла, подрузомевающего передачи параметров в `CMD` через команду `docker run`, доставляет немало проблем, ведь контейнеру нужно в том же `docker run` указать новый `ENTRYPOINT`. А он вообще был нужен, если контейнер, например, готовая среда для создания других контейнеров на основе этого (например, контейнер с интерпретатором python, содержащий все нужные плагины для mkdocs)? Чтобы избежать подобных проблем изучим как различаются и дополняют друг друга инструкции `ENTRYPOINT` и `CMD`.

Данная работа будет разделена на две основные части: само использование `CMD` и `ENTRYPOINT`, а вторая часть пояснит когда что использовать и как именно они работают вместе.

