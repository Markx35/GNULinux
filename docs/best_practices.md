# Взаимодействие CMD и ENTRYPOINT

## Основные правила

Как мы уже выяснили, обе инструкции CMD и ENTRYPOINT определяют какие команды исполняются на старте контейнера. Вот несколько правил, которые описывают взаимодействие инструкций:

* В Dockerfile должна быть хотя бы одна команда CMD и ENTRYPOINT;
* ENTRYPOINT используется если контейнер исполняемый и у него одна роль (например, сервер MySQL);
* CMD должен использоваться для описания стандартных аргументов для ENTRYPOINT или для дополнительных аргументов (таким образом, некоторые стандартные аргументы вроде флагов имеет смысл описать в ENTRYPOINT, если их точно не придется переназначать);
* CMD будет переназначен при исполнении контейнера с другими аргументами.

## Таблица взаимодействия параметров

|                              | Без `ENTRYPOINT`              | `ENTRYPOINT exec_entry p1_entry`  | `ENTRYPOINT [“exec_entry”, “p1_entry”]`          |
| ---------------------------- | ----------------------------- | --------------------------------- | ------------------------------------------------ |
| Без `CMD`                    | Ошибка                        | ` /bin/sh -c exec_entry p1_entry` | ` exec_entry p1_entry`                           |
| `CMD [“exec_cmd”, “p1_cmd”]` | ` exec_cmd p1_cmd`            | ` /bin/sh -c exec_entry p1_entry` | ` exec_entry p1_entry exec_cmd p1_cmd`           |
| ` CMD [“p1_cmd”, “p2_cmd”]`  | ` p1_cmd p2_cmd`              | ` /bin/sh -c exec_entry p1_entry` | ` exec_entry p1_entry p1_cmd p2_cmd`             |
| ` CMD exec_cmd p1_cmd`       | ` /bin/sh -c exec_cmd p1_cmd` | ` /bin/sh -c exec_entry p1_entry` | `exec_entry p1_entry /bin/sh -c exec_cmd p1_cmd` |

## Советы по использованию CMD и ENTRYPOINT

Как упоминалось выше, `ENTRYPOINT` надо использовать для исполняемых контейнеров, то есть готовых приложений вроде MySQL, Redis. Но точно не стоит использовать `ENTRYPOINT` для контейнеров общих, например, которые будут заготовкой для другого контейнера или использоваться при разработке.

Таким образом, для CI (Continous Integration) стоит использовать только `CMD`. Например, `CMD` запускает интерпретатор Python. Однако, если он нам не нужен, но нужен контейнер с Python и мы хотим установить пакеты через pip, то `CMD` легко переназначить и вначале установить пакеты через pip, а потом уже запускать Python.

!!! info "Пример"
    Посмотрим как используются параметры ENTRYPOINT и CMD на примере Gitlab CI. У нас есть вот такой вот `.gitlab-ci.yml`, с помощью которого собирается данная документация:
    
    ```yaml
    image: python:alpine
    
    before_script:
      - pip install mkdocs
      # Add your custom theme if not inside a theme_dir
      # (https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)
      - pip install mkdocs-material
      - pip install pymdown-extensions
    
    pages:
      script:
      - mkdocs build
      - mv site public
      artifacts:
        paths:
        - public
      only:
      - master
    ```
    
    Так как в образе `python:alpine` используется только `CMD ["python3"]`, то все что в секциях `before_script` и `script ` будет выполнено взамен `CMD` с использованием родного шела `/bin/sh -c` построчно.
    
    Если бы в образе был `ENTRYPOINT ["python3"]`, тогда все в указанных секциях бы пошло в интерпретатор Python 3 и у нас были бы ошиби. Исправить это можно, указывая вместо `image: python:alpine` такую конструкцию:
    
    ```yaml
    image:
      name: python:alpine
      entrypoint: ["/bin/sh", "-c"]
    ```
    
    Но это добавляет лишней головной боли, она же добавится и при написании Docker compose файлов.

